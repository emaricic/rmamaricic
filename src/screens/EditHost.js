import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Modal,
  StyleSheet,
  TextInput,
  ToastAndroid,
} from "react-native";
import { IconButton } from "react-native-paper";
import { useForm, Controller } from "react-hook-form";
import * as SQLite from "expo-sqlite";
import { Checkbox } from "react-native-paper";
import Loader from "../utils/loader";

const db = SQLite.openDatabase("database");

const EditHost = ({ modalIsOpen, closeModal, id }) => {
  const [checked, setChecked] = useState(false);
  const [state, setState] = useState({});

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const fetchData = () => {
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        // let innerJoin = await this.ExecuteQuery("SELECT users.id, users.first_name, users.last_name, c.country_name FROM users INNER JOIN country c on c.user_id = users.id
        "SELECT * FROM hosts WHERE id=?",
        [id], // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setState({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
  };

  const editValue = (data) => {
    console.log(id);
    console.log(data.name);

    db.transaction((tx) => {
      tx.executeSql(
        `UPDATE hosts SET name=?, OIB=?, address=? WHERE id=?`,
        [data.name, data.OIB, data.address, id],
        (txObj, resultSet) => {
          if (resultSet.rowsAffected > 0) {
            ToastAndroid.showWithGravity(
              "Uspješno uređeno!",
              ToastAndroid.SHORT,
              ToastAndroid.BOTTOM
            );
            closeModal();
          }
        }
      );
    });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Modal
      styles={{ alignItems: "center", justifyContent: "center" }}
      visible={modalIsOpen}
      animationType="slide"
    >
      <ScrollView
        style={{
          flex: 1,
          backgroundColor: "rgba(0,0,0,0.5)",
        }}
      >
        {state.data ? (
          <>
            <IconButton
              style={styles.icon}
              icon="close"
              color="#01264A"
              size={30}
              onPress={closeModal}
            />
            <Controller
              control={control}
              error={!!errors.agency}
              render={() => (
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Text style={styles.text}>Agencija </Text>
                  <Checkbox
                    status={checked ? "checked" : "unchecked"}
                    onPress={() => {
                      setChecked(!checked);
                    }}
                  />
                </View>
              )}
              name="agency"
            />
            <Text style={styles.text}>Naziv: </Text>
            <Controller
              control={control}
              rules={{ required: true, maxLength: 100, minLength: 3 }}
              error={!!errors.name}
              render={({ field: { onChange, onBlur, value } }) => (
                <TextInput
                  style={styles.input}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  defaultValue={state.data[0].name}
                />
              )}
              name="name"
            />
            {(errors.name && errors.name.type === "required" && (
              <Text
                style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}
              >
                Polje je obvezno
              </Text>
            )) ||
              (errors.name && errors.name.type === "minLength" && (
                <Text
                  style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}
                >
                  Polje mora imati minimalno 3 slova
                </Text>
              ))}
            <Text style={styles.text}>OIB: </Text>
            <Controller
              control={control}
              rules={{
                pattern: "[1-9][0-9]*|0",
                required: true,
                maxLength: 13,
                minLength: 13,
              }}
              error={!!errors.OIB}
              render={({ field: { onChange, onBlur, value } }) => (
                <TextInput
                  style={styles.input}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  defaultValue={String(state.data[0].OIB)}
                  keyboardType="number-pad"
                />
              )}
              name="OIB"
            />
            {(errors.OIB && errors.OIB.type === "required" && (
              <Text
                style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}
              >
                Polje je obvezno
              </Text>
            )) ||
              (errors.OIB && errors.OIB.type === "minLength" && (
                <Text
                  style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}
                >
                  Polje mora imati 13 znamenki
                </Text>
              )) ||
              (errors.OIB && errors.OIB.type === "maxLength" && (
                <Text
                  style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}
                >
                  Polje mora imati 13 znamenki
                </Text>
              )) ||
              (errors.OIB && errors.OIB.type === "pattern" && (
                <Text
                  style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}
                >
                  Polje ne smije zadržavati znakove
                </Text>
              ))}
            <Text style={styles.text}>Adresa: </Text>
            <Controller
              control={control}
              rules={{ required: true }}
              error={!!errors.address}
              render={({ field: { onChange, onBlur, value } }) => (
                <TextInput
                  style={styles.input}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  defaultValue={state.data[0].address}
                />
              )}
              name="address"
            />
            {errors.address && errors.address.type === "required" && (
              <Text
                style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}
              >
                Polje je obvezno
              </Text>
            )}
            {checked ? (
              <>
                <Text style={styles.text}>Telefon: </Text>
                <Controller
                  control={control}
                  rules={{ required: checked }}
                  error={!!errors.tel}
                  render={({ field: { onChange, onBlur, value } }) => (
                    <TextInput
                      style={styles.input}
                      onBlur={onBlur}
                      onChangeText={onChange}
                      value={value}
                      defaultValue={state.data[0].tel}
                      keyboardType="number-pad"
                    />
                  )}
                  name="tel"
                />
                {errors.tel && errors.tel.type === "required" && (
                  <Text
                    style={{
                      marginBottom: 10,
                      marginLeft: 10,
                      color: "#CC2828",
                    }}
                  >
                    Polje je obvezno
                  </Text>
                )}
                <Text style={styles.text}>Email: </Text>
                <Controller
                  control={control}
                  rules={{ required: checked }}
                  error={!!errors.email}
                  render={({ field: { onChange, onBlur, value } }) => (
                    <TextInput
                      style={styles.input}
                      onBlur={onBlur}
                      onChangeText={onChange}
                      value={value}
                      defaultValue={state.data[0].email}
                    />
                  )}
                  name="email"
                />
                {errors.email && errors.email.type === "required" && (
                  <Text
                    style={{
                      marginBottom: 10,
                      marginLeft: 10,
                      color: "#CC2828",
                    }}
                  >
                    Polje je obvezno
                  </Text>
                )}
              </>
            ) : null}
            <TouchableOpacity
              style={styles.button}
              title="Dodaj"
              onPress={handleSubmit(editValue)}
            >
              <Text style={styles.btnText}>Uredi</Text>
            </TouchableOpacity>
          </>
        ) : (
          <Loader />
        )}
      </ScrollView>
    </Modal>
  );
};

const styles = StyleSheet.create({
  icon: {
    marginVertical: 20,
    borderWidth: 1,
    borderColor: "#01264A",
    padding: 0,
    borderRadius: 10,
    alignSelf: "center",
  },
  input: {
    backgroundColor: "#F5F9FF",
    borderBottomWidth: 1,
    borderColor: "grey",
    padding: 10,
    fontSize: 20,
    marginBottom: 10,
    marginHorizontal: 10,
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 10,
    paddingHorizontal: 32,
    margin: 10,
    borderRadius: 8,
    elevation: 3,
    backgroundColor: "#01264A",
    color: "#845EC2",
  },
  btnText: { fontSize: 20, color: "white" },
  text: {
    margin: 10,
    fontSize: 20,
    lineHeight: 21,
    letterSpacing: 0.25,
    color: "#000",
  },
});

export default EditHost;
