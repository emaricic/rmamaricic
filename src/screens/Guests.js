import React, { useEffect, useMemo, useState } from "react";
import { Text, View, StyleSheet, FlatList, ToastAndroid } from "react-native";
import { IconButton } from "react-native-paper";
import AddGuest from "./AddGuest";
import EditGuest from "./EditGuest";
import * as SQLite from "expo-sqlite";
import Loader from "../utils/loader";

const db = SQLite.openDatabase("database");

const Guests = ({ navigation, currentHost }) => {
  const [modalIsOpen, setIsOpen] = useState(false);
  const [modalIsOpenEdit, setIsOpenEdit] = useState(false);
  const [state, setState] = useState({ data: null });
  const [editData, setEditData] = useState("");

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }
  function openEditModal() {
    setIsOpenEdit(true);
  }

  function closeEditModal() {
    setIsOpenEdit(false);
  }

  function getId(id) {
    setEditData(id);
  }

  const fetchData = () => {
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        "SELECT * FROM guests",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setState({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
  };

  const deleteRecord = (id) => {
    db.transaction((tx) => {
      tx.executeSql("DELETE FROM guests where id=?", [id], (tx, results) => {
        console.log("Results", results.rowsAffected);
        if (results.rowsAffected > 0) {
          ToastAndroid.showWithGravity(
            "Uspješno obrisano!",
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM
          );
          let newList = state.data.filter((data) => {
            if (data.id === id) return false;
            else return true;
          });
          setState({ data: newList });
        }
      });
    });
  };

  const data = useMemo(() => {
    let computedData = state.data;
    if (computedData && currentHost) {
      computedData = computedData
        .filter(function (item) {
          return item.hostId == currentHost;
        })
        .map(function ({ id, name }) {
          return { id, name };
        });
    }

    return computedData;
  }, [currentHost, state]);

  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql(
        "CREATE TABLE IF NOT EXISTS guests (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, hostId INTEGER)"
      );
    });
  }, []);

  useEffect(() => {
    fetchData();
  }, [modalIsOpen, modalIsOpenEdit]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#01264A",
      }}
    >
      <View style={styles.flex}>
        {data ? (
          data.length > 0 ? (
            <FlatList
              style={{ margin: 10 }}
              showsVerticalScrollIndicator={false}
              data={data}
              renderItem={({ item }) => (
                <>
                  <View style={styles.flatList}>
                    <Text style={styles.text}>Gost: {item.name}</Text>
                    <View style={styles.innerView}>
                      <IconButton
                        style={styles.icon}
                        icon="pen"
                        color="#01264A"
                        size={30}
                        onPress={() => {
                          getId(item.id);
                          openEditModal();
                        }}
                      />
                      <IconButton
                        style={styles.icon}
                        icon="delete"
                        color="#01264A"
                        size={30}
                        onPress={() => {
                          deleteRecord(item.id);
                        }}
                      />
                    </View>
                  </View>
                </>
              )}
              keyExtractor={(item) => item.id}
            />
          ) : (
            <View style={styles.flex}>
              <Text
                style={{
                  color: "#F5F9FF",
                  fontSize: 20,
                  alignSelf: "center",
                  marginTop: 50,
                }}
              >
                Iznajmljivač nema povezanog gosta!
              </Text>
            </View>
          )
        ) : (
          <Loader />
        )}

        <View style={styles.iconView}>
          <IconButton
            style={styles.icon}
            icon="plus-circle-outline"
            color="#96ABDA"
            size={50}
            onPress={openModal}
          />
        </View>
      </View>
      {modalIsOpen ? (
        <AddGuest modalIsOpen={modalIsOpen} closeModal={closeModal} />
      ) : null}
      {modalIsOpenEdit ? (
        <EditGuest
          editData={editData}
          modalIsOpen={modalIsOpenEdit}
          closeModal={closeEditModal}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  flatList: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginHorizontal: 4,
    marginVertical: 6,
    padding: 10,
    elevation: 3,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "#333",
    borderRadius: 8,
    backgroundColor: "#F5F9FF",
  },
  text: {
    fontSize: 16,
    color: "#001435",
  },
  icon: {
    padding: 0,
    marginTop: 0,
    marginHorizontal: 0,
  },
  flex: { display: "flex", flexDirection: "column", flex: 1 },
  iconView: {
    display: "flex",
    alignItems: "center",
    padding: 0,
    margin: 0,
    alignSelf: "flex-end",
  },
  innerView: {
    display: "flex",
    flexDirection: "row",
  },
});
export default Guests;
