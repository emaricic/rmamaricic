import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Modal,
  StyleSheet,
  TextInput,
  ToastAndroid,
  ScrollView,
} from "react-native";
import { IconButton, Divider } from "react-native-paper";
import { Picker } from "@react-native-picker/picker";
import { useForm, Controller } from "react-hook-form";
import * as SQLite from "expo-sqlite";
import DatePicker from "react-native-modern-datepicker";
import AddGuest from "./AddGuest";

const db = SQLite.openDatabase("database");

const AddBill = ({ modalIsOpen, closeModal }) => {
  const [hosts, setHosts] = useState([]);
  const [units, setUnits] = useState([]);
  const [guests, setGuests] = useState([]);

  const [formData, setFormData] = useState({
    host: "",
    guest: "",
    unit: "",
    paymentMethod: "",
    selectedDate: date,
    nightsStay: 0,
    reservationPrice: 0,
    nightPrice: 0,
    total: 0,
  });
  const [date, setDate] = useState(
    new Date().toISOString().replace("-", "/").split("T")[0].replace("-", "/")
  );
  const [openDate, setOpenDate] = useState(false);

  const [innerModalIsOpen, setInnerIsOpen] = useState(false);
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();

  function openInnerModal() {
    setInnerIsOpen(true);
  }

  function closeInnerModal() {
    setInnerIsOpen(false);
  }

  const handleOpenDate = () => {
    setOpenDate((openDate) => !openDate);
  };

  const fetchData = () => {
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        // let innerJoin = await this.ExecuteQuery("SELECT users.id, users.first_name, users.last_name, c.country_name FROM users INNER JOIN country c on c.user_id = users.id
        "SELECT * FROM hosts",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setHosts({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        // let innerJoin = await this.ExecuteQuery("SELECT users.id, users.first_name, users.last_name, c.country_name FROM users INNER JOIN country c on c.user_id = users.id
        "SELECT * FROM appartment",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setUnits({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
  };

  const fetchGuestData = () => {
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        "SELECT * FROM guests",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setGuests({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
  };

  const newItem = (data) => {
    db.transaction((tx) => {
      tx.executeSql(
        "INSERT INTO bill (guest, host, nightsPrice, nightsStay, paymentMethod, reservationPrice, selectedDate, total, unit) values (?,?,?,?,?,?,?,?,?)",
        [
          data.guest,
          data.host,
          data.nightPrice,
          data.nightsStay,
          data.paymentMethod,
          data.reservationPrice,
          date,
          formData.total,
          data.unit,
        ],
        (tx, results) => {
          console.log("Results", results.rowsAffected);
          if (results.rowsAffected > 0) {
            ToastAndroid.showWithGravity(
              "Uspješno dodano!",
              ToastAndroid.SHORT,
              ToastAndroid.BOTTOM
            );
            closeModal();
          } else Alert.alert("Failed....");
        }
      );
    });
  };

  useEffect(() => {
    fetchData();
    fetchGuestData();
  }, []);

  useEffect(() => {
    fetchGuestData();
  }, [modalIsOpen]);

  useEffect(() => {
    if (
      formData.reservationPrice > 0 &&
      formData.nightsStay > 0 &&
      formData.nightPrice > 0
    ) {
      var reservationPrice = parseInt(formData.reservationPrice);
      var nightsStay = parseInt(formData.nightsStay);
      var nightPrice = parseInt(formData.nightPrice);
      var total = reservationPrice + nightsStay * nightPrice;
      setFormData({
        ...formData,
        total: total,
      });
    }
  }, [formData.reservationPrice, formData.nightsStay, formData.nightPrice]);

  return (
    <Modal
      styles={{ alignItems: "center", justifyContent: "center" }}
      visible={modalIsOpen}
      animationType="slide"
    >
      <ScrollView
        style={{
          flex: 1,
          backgroundColor: "rgba(0,0,0,0.5)",
        }}
      >
        <IconButton
          style={styles.icon}
          icon="close"
          color="#01264A"
          size={30}
          onPress={closeModal}
        />
        <Text style={styles.text}>Iznajmljivač: </Text>

        {hosts.data ? (
          <View>
            <Controller
              value={formData.host}
              control={control}
              rules={{ required: "Odaberite iznajmjivača" }}
              error={!!errors.host}
              render={({
                field: { onChange, ...props },
                fieldState: { error },
              }) => (
                <>
                  <Picker
                    {...props}
                    selectedValue={formData.host}
                    onValueChange={(itemValue) => {
                      setFormData({ ...formData, host: itemValue });
                      onChange(itemValue);
                    }}
                  >
                    <Picker.Item value="" label="Odaberi iznajmljivača" />
                    {hosts.data.map((item, index) => {
                      return (
                        <Picker.Item
                          value={item.id}
                          label={item.name}
                          key={index}
                        />
                      );
                    })}
                  </Picker>
                  {error && (
                    <Text
                      style={{
                        marginBottom: 10,
                        marginLeft: 10,
                        color: "#CC2828",
                      }}
                    >
                      {error.message || "Error"}
                    </Text>
                  )}
                </>
              )}
              name="host"
            />
          </View>
        ) : null}

        <Text style={styles.text}>Smještajna jedinica: </Text>

        {units.data ? (
          <View>
            <Controller
              value={formData.unit}
              control={control}
              rules={{ required: "Odaberite smještajnu jedinicu" }}
              error={!!errors.unit}
              render={({
                field: { onChange, ...props },
                fieldState: { error },
              }) => (
                <>
                  <Picker
                    {...props}
                    selectedValue={formData.unit}
                    onValueChange={(itemValue) => {
                      setFormData({ ...formData, unit: itemValue });
                      onChange(itemValue);
                    }}
                  >
                    <Picker.Item value="" label="Odaberi smještajnu jedinicu" />
                    {units.data.map((item, index) => {
                      return (
                        <Picker.Item
                          value={item.id}
                          label={item.name}
                          key={index}
                        />
                      );
                    })}
                  </Picker>
                  {error && (
                    <Text
                      style={{
                        marginBottom: 10,
                        marginLeft: 10,
                        color: "#CC2828",
                      }}
                    >
                      {error.message || "Error"}
                    </Text>
                  )}
                </>
              )}
              name="unit"
            />
          </View>
        ) : null}

        <Text style={styles.text}>Gost: </Text>

        {guests.data ? (
          <View>
            <Controller
              value={formData.guest}
              control={control}
              rules={{ required: "Odaberite gosta" }}
              error={!!errors.guest}
              render={({
                field: { onChange, ...props },
                fieldState: { error },
              }) => (
                <>
                  <View style={{ display: "flex", flexDirection: "row" }}>
                    <Picker
                      {...props}
                      selectedValue={formData.guest}
                      onValueChange={(itemValue) => {
                        setFormData({ ...formData, guest: itemValue });
                        onChange(itemValue);
                      }}
                      style={{ flex: 1, marginRight: 5 }}
                    >
                      <Picker.Item value="" label="Odaberi gosta" />
                      {guests.data.map((item, index) => {
                        return (
                          <Picker.Item
                            value={item.id}
                            label={item.name}
                            key={index}
                          />
                        );
                      })}
                    </Picker>
                    <View style={styles.iconView}>
                      <IconButton
                        style={styles.icon}
                        icon="plus-circle-outline"
                        color="#01264A"
                        size={20}
                        onPress={openInnerModal}
                      />
                    </View>
                  </View>
                  {error && (
                    <Text
                      style={{
                        marginBottom: 10,
                        marginLeft: 10,
                        color: "#CC2828",
                      }}
                    >
                      {error.message || "Error"}
                    </Text>
                  )}
                </>
              )}
              name="guest"
            />
          </View>
        ) : null}

        <Text style={styles.text}>Odaberi datum: </Text>

        <TouchableOpacity
          style={styles.secondaryButton}
          title={!openDate ? "Odaberi" : "Zatvori"}
          onPress={handleOpenDate}
        >
          <Text style={styles.btnSeondaryText}>
            {!openDate ? "Odaberi" : "Zatvori"}
          </Text>
        </TouchableOpacity>
        {openDate ? (
          <View style={{ margin: 10 }}>
            <DatePicker
              options={{
                backgroundColor: "#090C08",
                textHeaderColor: "#845EC2",
                textDefaultColor: "#FBEAFF",
                selectedTextColor: "#fff",
                mainColor: "#845EC2",
                textSecondaryColor: "#B39CD0",
                borderColor: "rgba(122, 146, 165, 0.1)",
              }}
              onSelectedChange={(date) => setDate(date.slice(0, 10))}
              style={{ borderRadius: 10 }}
            />
          </View>
        ) : null}

        <Text style={styles.text}>Način plaćanja: </Text>
        <View>
          <Controller
            value={formData.paymentMethod}
            control={control}
            rules={{ required: "Odaberite način plačanja" }}
            error={!!errors.paymentMethod}
            render={({
              field: { onChange, ...props },
              fieldState: { error },
            }) => (
              <>
                <Picker
                  {...props}
                  selectedValue={formData.paymentMethod}
                  onValueChange={(itemValue) => {
                    setFormData({ ...formData, paymentMethod: itemValue });
                    onChange(itemValue);
                  }}
                >
                  <Picker.Item value="" label="Odaberi način plaćanja" />
                  <Picker.Item value="Gotovina" label="Gotovina" />
                  <Picker.Item value="Transakcijsko" label="Transakcijsko" />
                  <Picker.Item value="Kartica" label="Kartica" />
                  <Picker.Item value="Ostalo" label="Ostalo" />
                </Picker>
                {error && (
                  <Text
                    style={{
                      marginBottom: 10,
                      marginLeft: 10,
                      color: "#CC2828",
                    }}
                  >
                    {error.message || "Error"}
                  </Text>
                )}
              </>
            )}
            name="paymentMethod"
          />
        </View>

        <Text style={styles.text}>Broj noćenja: </Text>
        <Controller
          type="number"
          control={control}
          rules={{ required: true }}
          error={!!errors.nightsStay}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={(realValue) => {
                onChange(realValue);
                setFormData({ ...formData, nightsStay: realValue });
              }}
              value={value}
              keyboardType="number-pad"
            />
          )}
          name="nightsStay"
        />
        {errors.nightsStay && errors.nightsStay.type === "required" && (
          <Text style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}>
            Unesite broj noćenja
          </Text>
        )}

        <Text style={styles.text}>Cijena rezervacije: </Text>
        <Controller
          type="number"
          control={control}
          rules={{ required: true }}
          error={!!errors.reservationPrice}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={(realValue) => {
                onChange(realValue);
                setFormData({ ...formData, reservationPrice: realValue });
              }}
              value={value}
              keyboardType="number-pad"
            />
          )}
          name="reservationPrice"
        />
        {errors.reservationPrice &&
          errors.reservationPrice.type === "required" && (
            <Text
              style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}
            >
              Polje je obvezno
            </Text>
          )}

        <Text style={styles.text}>Cijena noćenja: </Text>
        <Controller
          type="number"
          control={control}
          rules={{ required: true }}
          error={!!errors.nightPrice}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={(realValue) => {
                onChange(realValue);
                setFormData({ ...formData, nightPrice: realValue });
              }}
              value={value}
              keyboardType="number-pad"
            />
          )}
          name="nightPrice"
        />
        {errors.nightPrice && errors.nightPrice.type === "required" && (
          <Text style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}>
            Polje je obvezno
          </Text>
        )}
        <Divider />
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Text style={styles.text}>Ukupno: </Text>
          <Text style={{ color: "#CC2828", fontSize: 20 }}>
            {formData.total}kn
          </Text>
        </View>

        <Divider />

        <TouchableOpacity
          style={styles.button}
          title="Dodaj"
          onPress={handleSubmit(newItem)}
        >
          <Text style={styles.btnText}>Dodaj</Text>
        </TouchableOpacity>
        {innerModalIsOpen ? (
          <AddGuest
            modalIsOpen={innerModalIsOpen}
            closeModal={closeInnerModal}
          />
        ) : null}
      </ScrollView>
    </Modal>
  );
};

const styles = StyleSheet.create({
  icon: {
    marginVertical: 20,
    borderWidth: 1,
    borderColor: "#01264A",
    padding: 0,
    borderRadius: 10,
    alignSelf: "center",
  },
  input: {
    backgroundColor: "#F5F9FF",
    borderBottomWidth: 1,
    borderColor: "#001435",
    padding: 10,
    fontSize: 20,
    marginBottom: 10,
    marginHorizontal: 10,
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 10,
    paddingHorizontal: 32,
    margin: 10,
    borderRadius: 8,
    elevation: 3,
    backgroundColor: "#1E3F66",
    color: "#01264A",
  },
  secondaryButton: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 6,
    paddingHorizontal: 16,
    margin: 10,
    borderRadius: 8,
    elevation: 3,
    backgroundColor: "#B39CD0",
    color: "#01264A",
  },
  btnText: { fontSize: 20, color: "white" },
  btnSeondaryText: { fontSize: 16, color: "white" },
  flatList: {
    borderColor: "#4B4453",
    borderWidth: 1,
    margin: 10,
    padding: 10,
    borderRadius: 8,
    backgroundColor: "#9B89B3",
  },
  text: {
    margin: 10,
    fontSize: 20,
    lineHeight: 21,
    letterSpacing: 0.25,
    color: "#000",
  },
  flex: { display: "flex", flexDirection: "column", flex: 1 },
});

export default AddBill;
