import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

const Tab3 = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "blue" }}>
      <Text>Tab3</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    color: "white",
  },
});

export default Tab3;
