import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Modal,
  StyleSheet,
  TextInput,
  ToastAndroid,
} from "react-native";
import { IconButton } from "react-native-paper";
import { Picker } from "@react-native-picker/picker";
import { useForm, Controller } from "react-hook-form";
import * as SQLite from "expo-sqlite";

const db = SQLite.openDatabase("database");

const AddGuest = ({ modalIsOpen, closeModal }) => {
  const [hosts, setHosts] = useState([]);
  const [host, setHost] = useState("");
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const newItem = (data) => {
    db.transaction((tx) => {
      tx.executeSql(
        "INSERT INTO guests (name, hostId) values (?,?)",
        [data.name, data.host],
        (tx, results) => {
          console.log("Results", results.rowsAffected);
          if (results.rowsAffected > 0) {
            ToastAndroid.showWithGravity(
              "Uspješno dodano!",
              ToastAndroid.SHORT,
              ToastAndroid.BOTTOM
            );
            closeModal();
          } else Alert.alert("Failed....");
        }
      );
    });
  };

  const fetchHostData = () => {
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        // let innerJoin = await this.ExecuteQuery("SELECT users.id, users.first_name, users.last_name, c.country_name FROM users INNER JOIN country c on c.user_id = users.id
        "SELECT * FROM hosts",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setHosts({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
  };

  useEffect(() => {
    fetchHostData();
  }, []);

  return (
    <Modal
      styles={{ alignItems: "center", justifyContent: "center" }}
      visible={modalIsOpen}
      animationType="slide"
    >
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          backgroundColor: "rgba(0,0,0,0.5)",
        }}
      >
        <IconButton
          style={styles.icon}
          icon="close"
          color="#01264A"
          size={30}
          onPress={closeModal}
        />
        <Text style={styles.text}>Ime i prezime: </Text>

        <Controller
          control={control}
          rules={{ required: true, maxLength: 100, minLength: 3 }}
          error={!!errors.name}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
            />
          )}
          name="name"
        />
        {(errors.name && errors.name.type === "required" && (
          <Text style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}>
            Polje je obvezno
          </Text>
        )) ||
          (errors.name && errors.name.type === "minLength" && (
            <Text
              style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}
            >
              Polje mora imati minimalno 3 slova
            </Text>
          ))}

        <Text style={styles.text}>Iznajmljivač: </Text>

        {hosts.data ? (
          <View>
            <Controller
              value={host}
              control={control}
              rules={{ required: "Odaberite iznajmjivača" }}
              error={!!errors.host}
              render={({
                field: { onChange, ...props },
                fieldState: { error },
              }) => (
                <>
                  <Picker
                    {...props}
                    selectedValue={host}
                    onValueChange={(itemValue) => {
                      setHost(itemValue);
                      onChange(itemValue);
                    }}
                  >
                    <Picker.Item value="" label="Odaberi iznajmljivača" />
                    {hosts.data.map((item, index) => {
                      return (
                        <Picker.Item
                          value={item.id}
                          label={item.name}
                          key={index}
                        />
                      );
                    })}
                  </Picker>
                  {error && (
                    <Text
                      style={{
                        marginBottom: 10,
                        marginLeft: 10,
                        color: "#CC2828",
                      }}
                    >
                      {error.message || "Error"}
                    </Text>
                  )}
                </>
              )}
              name="host"
            />
          </View>
        ) : null}
        <TouchableOpacity
          style={styles.button}
          title="Dodaj"
          onPress={handleSubmit(newItem)}
        >
          <Text style={styles.btnText}>Dodaj</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  icon: {
    marginVertical: 20,
    borderWidth: 1,
    borderColor: "#01264A",
    padding: 0,
    borderRadius: 10,
    alignSelf: "center",
  },
  input: {
    backgroundColor: "#F5F9FF",
    borderBottomWidth: 1,
    borderColor: "#001435",
    padding: 10,
    fontSize: 20,
    marginBottom: 10,
    marginHorizontal: 10,
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 10,
    paddingHorizontal: 32,
    margin: 10,
    borderRadius: 8,
    elevation: 3,
    backgroundColor: "#01264A",
    color: "#845EC2",
  },
  btnText: { fontSize: 20, color: "white" },
  text: {
    margin: 10,
    fontSize: 20,
    lineHeight: 21,
    letterSpacing: 0.25,
    color: "#000",
  },
});

export default AddGuest;
