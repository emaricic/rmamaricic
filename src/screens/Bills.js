import React, { useEffect, useMemo, useState } from "react";
import { Text, View, StyleSheet, FlatList, ToastAndroid } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { IconButton } from "react-native-paper";
import IconRight from "../../assets/plus-circle-outline.svg";
import * as SQLite from "expo-sqlite";
import AddBill from "./AddBill";
import Loader from "../utils/loader";
const db = SQLite.openDatabase("database");

const Bills = ({ navigation, currentHost }) => {
  const [state, setState] = useState({ data: null });
  const [hosts, setHosts] = useState([]);
  const [guests, setGuests] = useState([]);
  const [units, setUnits] = useState([]);

  const [modalIsOpen, setIsOpen] = useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }
  const fetchData = () => {
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        // let innerJoin = await this.ExecuteQuery("SELECT users.id, users.first_name, users.last_name, c.country_name FROM users INNER JOIN country c on c.user_id = users.id
        "SELECT * FROM bill",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setState({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        // let innerJoin = await this.ExecuteQuery("SELECT users.id, users.first_name, users.last_name, c.country_name FROM users INNER JOIN country c on c.user_id = users.id
        "SELECT * FROM hosts",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setHosts({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        "SELECT * FROM guests",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setGuests({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        // let innerJoin = await this.ExecuteQuery("SELECT users.id, users.first_name, users.last_name, c.country_name FROM users INNER JOIN country c on c.user_id = users.id
        "SELECT * FROM appartment",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setUnits({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
  };

  const deleteRecord = (id) => {
    db.transaction((tx) => {
      tx.executeSql("DELETE FROM bill where id=?", [id], (tx, results) => {
        console.log("Results", results.rowsAffected);
        if (results.rowsAffected > 0) {
          ToastAndroid.showWithGravity(
            "Uspješno obrisano!",
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM
          );
          let newList = state.data.filter((data) => {
            if (data.id === id) return false;
            else return true;
          });
          setState({ data: newList });
        }
      });
    });
  };

  const data = useMemo(() => {
    let computedData = state.data;
    if (computedData && currentHost) {
      computedData = computedData
        .filter(function (item) {
          return item.host == currentHost;
        })
        .map(function ({
          id,
          host,
          guest,
          paymentMethod,
          reservationPrice,
          selectedDate,
          total,
          nightsStay,
          nightsPrice,
          unit,
        }) {
          return {
            id,
            host,
            guest,
            paymentMethod,
            reservationPrice,
            selectedDate,
            total,
            nightsStay,
            nightsPrice,
            unit,
          };
        });
    }

    return computedData;
  }, [currentHost, state]);

  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql(
        "CREATE TABLE IF NOT EXISTS bill (id INTEGER PRIMARY KEY AUTOINCREMENT, guest INT, host INT, nightsPrice TEXT, nightsStay TEXT, paymentMethod TEXT, reservationPrice TEXT, selectedDate TEXT, total INT, unit INT)"
      );
    });
  }, []);

  useEffect(() => {
    fetchData();
  }, [modalIsOpen]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#01264A",
      }}
    >
      <View style={styles.flex}>
        {data ? (
          data.length > 0 ? (
            <FlatList
              style={{ margin: 10 }}
              showsVerticalScrollIndicator={false}
              data={data}
              renderItem={({ item }) => (
                <View style={styles.flatList}>
                  <View>
                    <FlatList
                      showsVerticalScrollIndicator={false}
                      data={hosts.data}
                      renderItem={(host) => (
                        <>
                          {host.item.id === item.host ? (
                            <Text style={styles.text}>
                              Iznajmljivač: {host.item.name}
                            </Text>
                          ) : null}
                        </>
                      )}
                      listKey={(item, index) => `_key${index.toString()}`}
                      keyExtractor={(item, index) => `_key${index.toString()}`}
                    />
                    <FlatList
                      showsVerticalScrollIndicator={false}
                      data={guests.data}
                      renderItem={(guest) => (
                        <>
                          {guest.item.id === item.guest ? (
                            <Text style={styles.text}>
                              Gost: {guest.item.name}
                            </Text>
                          ) : null}
                        </>
                      )}
                      listKey={(item, index) => `_key${index.toString()}`}
                      keyExtractor={(item, index) => `_key${index.toString()}`}
                    />
                    <FlatList
                      showsVerticalScrollIndicator={false}
                      data={units.data}
                      renderItem={(unit) => (
                        <>
                          {unit.item.id === item.unit ? (
                            <Text style={styles.text}>
                              Smještajna jedinica: {unit.item.name}
                            </Text>
                          ) : null}
                        </>
                      )}
                      keyExtractor={(unit) => unit.id}
                    />
                    <Text style={styles.text}>
                      Način plaćanja: {item.paymentMethod}
                    </Text>
                    <Text style={styles.text}>
                      Cijena rezervacije: {item.reservationPrice}
                    </Text>
                    <Text style={styles.text}>Noći: {item.nightsStay}</Text>

                    <Text style={styles.text}>
                      Cijena noći: {item.nightsPrice}
                    </Text>

                    <Text style={styles.text}>Datum: {item.selectedDate}</Text>
                    <Text style={styles.text}>Ukupno: {item.total}kn</Text>
                  </View>
                  <View
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "flex-end",
                      flex: 1,
                    }}
                  >
                    <IconButton
                      style={styles.icon}
                      icon="delete"
                      color="#01264A"
                      size={30}
                      onPress={() => {
                        deleteRecord(item.id);
                      }}
                    />
                  </View>
                </View>
              )}
              keyExtractor={(item) => item.id}
            />
          ) : (
            <View style={styles.flex}>
              <Text
                style={{
                  color: "#F5F9FF",
                  fontSize: 20,
                  alignSelf: "center",
                  marginTop: 50,
                }}
              >
                Iznajmljivač nema računa!
              </Text>
            </View>
          )
        ) : (
          <Loader />
        )}

        <View style={styles.iconView}>
          <IconButton
            style={styles.icon}
            icon="plus-circle-outline"
            color="#96ABDA"
            size={50}
            onPress={openModal}
          />
        </View>
      </View>
      {modalIsOpen ? (
        <AddBill modalIsOpen={modalIsOpen} closeModal={closeModal} />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  flatList: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 4,
    marginVertical: 6,
    padding: 10,
    elevation: 3,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "#000",
    borderRadius: 8,
    backgroundColor: "#F5F9FF",
  },
  text: {
    fontSize: 16,
    color: "#001435",
  },
  icon: {
    padding: 0,
    marginTop: 0,
    marginHorizontal: 0,
  },
  flex: { display: "flex", flexDirection: "column", flex: 1 },
  iconView: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: 0,
    margin: 0,
    alignSelf: "flex-end",
  },
  innerView: {
    display: "flex",
    flexDirection: "row",
  },
});

export default Bills;
