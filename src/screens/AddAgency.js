import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Modal,
  StyleSheet,
  TextInput,
  ToastAndroid,
} from "react-native";
import { IconButton } from "react-native-paper";
import { useForm, Controller } from "react-hook-form";
import * as SQLite from "expo-sqlite";

const db = SQLite.openDatabase("database");

const AddAgency = ({ modalIsOpen, closeModal }) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      name: "",
    },
  });

  const newItem = (data) => {
    db.transaction((tx) => {
      tx.executeSql(
        "INSERT INTO agencies (name,OIB,address,tel,email) values (?,?,?,?,?)",
        [data.name, data.OIB, data.address, data.tel, data.email],
        (tx, results) => {
          console.log("Results", results.rowsAffected);
          if (results.rowsAffected > 0) {
            ToastAndroid.showWithGravity(
              "Uspješno dodano!",
              ToastAndroid.SHORT,
              ToastAndroid.BOTTOM
            );
            closeModal();
          } else Alert.alert("Failed....");
        }
      );
    });
  };

  return (
    <Modal
      styles={{ alignItems: "center", justifyContent: "center" }}
      visible={modalIsOpen}
      animationType="slide"
    >
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          backgroundColor: "rgba(0,0,0,0.5)",
        }}
      >
        <IconButton
          style={styles.icon}
          icon="close"
          color="#845EC2"
          size={30}
          onPress={closeModal}
        />
        <Text style={styles.text}>Naziv: </Text>

        <Controller
          control={control}
          rules={{ required: true, maxLength: 100, minLength: 3 }}
          error={!!errors.name}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
            />
          )}
          name="name"
        />
        {(errors.name && errors.name.type === "required" && (
          <Text style={{ marginBottom: 10, marginLeft: 10, color: "red" }}>
            Polje je obvezno
          </Text>
        )) ||
          (errors.name && errors.name.type === "minLength" && (
            <Text style={{ marginBottom: 10, marginLeft: 10, color: "red" }}>
              Polje mora imati minimalno 3 slova
            </Text>
          ))}

        <Text style={styles.text}>OIB: </Text>

        <Controller
          control={control}
          rules={{
            pattern: "[1-9][0-9]*|0",
            required: true,
            maxLength: 13,
            minLength: 13,
          }}
          error={!!errors.OIB}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
              keyboardType="number-pad"
            />
          )}
          name="OIB"
        />
        {(errors.OIB && errors.OIB.type === "required" && (
          <Text style={{ marginBottom: 10, marginLeft: 10, color: "red" }}>
            Polje je obvezno
          </Text>
        )) ||
          (errors.OIB && errors.OIB.type === "minLength" && (
            <Text style={{ marginBottom: 10, marginLeft: 10, color: "red" }}>
              Polje mora imati 13 znamenki
            </Text>
          )) ||
          (errors.OIB && errors.OIB.type === "maxLength" && (
            <Text style={{ marginBottom: 10, marginLeft: 10, color: "red" }}>
              Polje mora imati 13 znamenki
            </Text>
          )) ||
          (errors.OIB && errors.OIB.type === "pattern" && (
            <Text style={{ marginBottom: 10, marginLeft: 10, color: "red" }}>
              Polje ne smije zadržavati znakove
            </Text>
          ))}
        <Text style={styles.text}>Adresa: </Text>

        <Controller
          control={control}
          rules={{ required: true }}
          error={!!errors.address}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
            />
          )}
          name="address"
        />
        {errors.address && errors.address.type === "required" && (
          <Text style={{ marginBottom: 10, marginLeft: 10, color: "red" }}>
            Polje je obvezno
          </Text>
        )}

        <Text style={styles.text}>Telefon: </Text>

        <Controller
          control={control}
          rules={{ required: true }}
          error={!!errors.tel}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
            />
          )}
          name="tel"
        />
        {errors.tel && errors.tel.type === "required" && (
          <Text style={{ marginBottom: 10, marginLeft: 10, color: "red" }}>
            Polje je obvezno
          </Text>
        )}

        <Text style={styles.text}>Email: </Text>

        <Controller
          control={control}
          rules={{ required: true }}
          error={!!errors.email}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
            />
          )}
          name="email"
        />
        {errors.email && errors.email.type === "required" && (
          <Text style={{ marginBottom: 10, marginLeft: 10, color: "red" }}>
            Polje je obvezno
          </Text>
        )}

        <TouchableOpacity
          style={styles.button}
          title="Dodaj"
          onPress={handleSubmit(newItem)}
        >
          <Text style={styles.btnText}>Dodaj</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  icon: {
    marginVertical: 20,
    borderWidth: 1,
    borderColor: "#845EC2",
    padding: 0,
    borderRadius: 10,
    alignSelf: "center",
  },
  input: {
    backgroundColor: "white",
    borderBottomWidth: 1,
    borderColor: "grey",
    padding: 10,
    fontSize: 20,
    marginBottom: 10,
    marginHorizontal: 10,
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 10,
    paddingHorizontal: 32,
    margin: 10,
    borderRadius: 8,
    elevation: 3,
    backgroundColor: "#845EC2",
    color: "#845EC2",
  },
  btnText: { fontSize: 20, color: "white" },
  text: {
    margin: 10,
    fontSize: 20,
    lineHeight: 21,
    letterSpacing: 0.25,
    color: "white",
  },
});

export default AddAgency;
