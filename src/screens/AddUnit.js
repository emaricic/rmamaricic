import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Modal,
  StyleSheet,
  TextInput,
  ToastAndroid,
} from "react-native";
import { IconButton } from "react-native-paper";
import { useForm, Controller } from "react-hook-form";
import * as SQLite from "expo-sqlite";

const db = SQLite.openDatabase("database");

const AddUnit = ({ modalIsOpen, closeModal, id }) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const newItem = (data) => {
    db.transaction((tx) => {
      tx.executeSql(
        "INSERT INTO appartment (name,hostId) values (?,?)",
        [data.name, id],
        (tx, results) => {
          console.log("Results", results.rowsAffected);
          if (results.rowsAffected > 0) {
            ToastAndroid.showWithGravity(
              "Uspješno obrisano!",
              ToastAndroid.SHORT,
              ToastAndroid.BOTTOM
            );
            closeModal();
          } else Alert.alert("Failed....");
        }
      );
    });
  };

  return (
    <Modal
      styles={{ alignItems: "center", justifyContent: "center" }}
      visible={modalIsOpen}
      animationType="slide"
    >
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          backgroundColor: "rgba(0,0,0,0.5)",
        }}
      >
        <IconButton
          style={styles.icon}
          icon="close"
          color="#01264A"
          size={30}
          onPress={closeModal}
        />
        <Text style={styles.text}>Naziv smještajne jedinice: </Text>

        <Controller
          control={control}
          rules={{ required: true }}
          error={!!errors.name}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
            />
          )}
          name="name"
        />
        {errors.name && errors.name.type === "required" && (
          <Text style={{ marginBottom: 10, marginLeft: 10, color: "#CC2828" }}>
            Polje je obvezno
          </Text>
        )}
        <TouchableOpacity
          style={styles.button}
          title="Dodaj"
          onPress={handleSubmit(newItem)}
        >
          <Text style={styles.btnText}>Dodaj</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  icon: {
    marginVertical: 20,
    borderWidth: 1,
    borderColor: "#01264A",
    padding: 0,
    borderRadius: 10,
    alignSelf: "center",
  },
  input: {
    backgroundColor: "#F5F9FF",
    borderBottomWidth: 1,
    borderColor: "grey",
    padding: 10,
    fontSize: 20,
    marginBottom: 10,
    marginHorizontal: 10,
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 10,
    paddingHorizontal: 32,
    margin: 10,
    borderRadius: 8,
    elevation: 3,
    backgroundColor: "#01264A",
    color: "#845EC2",
  },
  btnText: { fontSize: 20, color: "white" },
  text: {
    margin: 10,
    fontSize: 20,
    lineHeight: 21,
    letterSpacing: 0.25,
    color: "#000",
  },
});

export default AddUnit;
