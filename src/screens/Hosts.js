import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, FlatList, ToastAndroid } from "react-native";
import { IconButton } from "react-native-paper";
import * as SQLite from "expo-sqlite";
import AddUnit from "./AddUnit";
import EditHost from "./EditHost";
import AddHost from "./AddHost";
import Loader from "../utils/loader";

const db = SQLite.openDatabase("database");

const Hosts = ({ navigation }) => {
  const [state, setState] = useState([]);
  const [unit, setUnit] = useState([]);
  const [modalIsOpen, setIsOpen] = useState(false);
  const [modalIsOpenEdit, setIsOpenEdit] = useState(false);

  const [modalIsOpenUnit, setIsOpenUnit] = useState(false);

  const [id, setId] = useState("");

  const getId = (id) => {
    setId(id);
  };

  function openEditModal() {
    setIsOpenEdit(true);
  }

  function closeEditModal() {
    setIsOpenEdit(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }
  function openModalUnit() {
    setIsOpenUnit(true);
  }

  function closeModalUnit() {
    setIsOpenUnit(false);
  }

  const fetchData = () => {
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        // let innerJoin = await this.ExecuteQuery("SELECT users.id, users.first_name, users.last_name, c.country_name FROM users INNER JOIN country c on c.user_id = users.id
        "SELECT * FROM hosts",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setState({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        // let innerJoin = await this.ExecuteQuery("SELECT users.id, users.first_name, users.last_name, c.country_name FROM users INNER JOIN country c on c.user_id = users.id
        "SELECT * FROM appartment",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setUnit({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
  };

  const deleteRecord = (id) => {
    db.transaction((tx) => {
      tx.executeSql("DELETE FROM hosts where id=?", [id], (tx, results) => {
        console.log("Results", results.rowsAffected);
        if (results.rowsAffected > 0) {
          ToastAndroid.showWithGravity(
            "Uspješno obrisano!",
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM
          );
          let newList = state.data.filter((data) => {
            if (data.id === id) return false;
            else return true;
          });
          setState({ data: newList });
        }
      });
    });
  };

  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql(
        "CREATE TABLE IF NOT EXISTS hosts (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, OIB INT(13) NOT NULL, address TEXT NOT NULL,email TEXT, tel TEXT)"
      );
    });
    db.transaction((tx) => {
      tx.executeSql(
        "CREATE TABLE IF NOT EXISTS appartment (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, hostId INTEGER)"
      );
    });
  }, []);

  useEffect(() => {
    fetchData();
  }, [modalIsOpen, modalIsOpenUnit, modalIsOpenEdit]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#01264A",
      }}
    >
      {state && unit.data ? (
        <View style={styles.flex}>
          <FlatList
            style={{ margin: 10 }}
            showsVerticalScrollIndicator={false}
            data={state.data}
            renderItem={({ item }) => (
              <View style={styles.flatList}>
                <View>
                  <Text style={styles.text}>
                    Ime
                    <Text style={styles.text}>
                      {item.tel && item.email ? " agencije" : null}
                    </Text>
                    : {item.name}
                  </Text>
                  <Text style={styles.text}>OIB: {item.OIB}</Text>
                  <Text style={styles.text}>Adresa: {item.address}</Text>
                  <Text style={styles.text}>Smještajne jedinice:</Text>
                  <FlatList
                    data={unit.data}
                    renderItem={(unitItem) => (
                      <View>
                        {item.id === unitItem.item.hostId ? (
                          <View
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <Text style={{ marginRight: 4, color: "white" }}>
                              {"\u2022"}
                            </Text>
                            <Text style={styles.text}>
                              {unitItem.item.name}
                            </Text>
                          </View>
                        ) : null}
                      </View>
                    )}
                    listKey={(item, index) => `_key${index.toString()}`}
                    keyExtractor={(item, index) => `_key${index.toString()}`}
                  />
                  {item.email ? (
                    <Text style={styles.text}>Email: {item.email}</Text>
                  ) : null}

                  {item.tel ? (
                    <Text style={styles.text}>Telefon: {item.tel}</Text>
                  ) : null}
                </View>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "flex-end",
                    flex: 1,
                  }}
                >
                  <View
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <Text style={styles.text}>Dodaj jedinicu:</Text>
                    <IconButton
                      icon="plus-circle-outline"
                      color="#01264A"
                      size={30}
                      onPress={() => {
                        getId(item.id);
                        openModalUnit();
                      }}
                    />
                  </View>
                  <IconButton
                    icon="pen"
                    color="#01264A"
                    size={30}
                    onPress={() => {
                      getId(item.id);
                      openEditModal();
                    }}
                  />
                  <IconButton
                    icon="delete"
                    color="#01264A"
                    size={30}
                    onPress={() => {
                      deleteRecord(item.id);
                    }}
                  />
                </View>
              </View>
            )}
            keyExtractor={(item) => item.id}
          />

          <View style={styles.iconView}>
            <IconButton
              style={styles.icon}
              icon="plus-circle-outline"
              color="#96ABDA"
              size={50}
              onPress={openModal}
            />
          </View>
        </View>
      ) : (
        <Loader />
      )}
      {modalIsOpen ? (
        <AddHost modalIsOpen={modalIsOpen} closeModal={closeModal} />
      ) : null}
      {modalIsOpenUnit ? (
        <AddUnit
          id={id}
          modalIsOpen={modalIsOpenUnit}
          closeModal={closeModalUnit}
        />
      ) : null}
      {modalIsOpenEdit ? (
        <EditHost
          id={id}
          modalIsOpen={modalIsOpenEdit}
          closeModal={closeEditModal}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  flatList: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 4,
    marginVertical: 6,
    padding: 10,
    elevation: 3,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "#000",
    borderRadius: 8,
    backgroundColor: "#F5F9FF",
  },
  text: {
    fontSize: 16,
    color: "#001435",
  },
  icon: {
    padding: 0,
    marginTop: 0,
    marginHorizontal: 0,
  },
  flex: { display: "flex", flexDirection: "column", flex: 1 },
  iconView: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 0,
    margin: 0,
    alignSelf: "flex-end",
  },
});

export default Hosts;
