import React, { useEffect } from "react";
import {
  BottomNavigation,
  BottomNavigationTab,
  Divider,
} from "@ui-kitten/components";

import Guests from "../screens/Guests";
import Bills from "../screens/Bills";
import { View } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

const Tab = createBottomTabNavigator();

const BottomTabBar = ({ navigation, state, setTabName }) => {
  return (
    <View>
      <Divider />
      <BottomNavigation
        backgroundColor="000"
        indicatorStyle={{ backgroundColor: "#8CA0CE", height: 4 }}
        selectedIndex={state.index}
        onSelect={(index) => {
          navigation.navigate(state.routeNames[index]);
          setTabName(state.routeNames[index]);
        }}
      >
        <BottomNavigationTab title="Gosti" />
        <BottomNavigationTab title="Računi" />
      </BottomNavigation>
    </View>
  );
};

const BottomNavigator = ({ setTabName, currentHost }) => {
  return (
    <Tab.Navigator
      tabBar={(props) => <BottomTabBar setTabName={setTabName} {...props} />}
      screenOptions={{ headerShown: false }}
    >
      <Tab.Screen
        name="Gosti"
        children={() => <Guests currentHost={currentHost} />}
      />
      <Tab.Screen
        name="Računi"
        children={() => <Bills currentHost={currentHost} />}
      />
    </Tab.Navigator>
  );
};

export { BottomNavigator };
