import React from "react";
import { DrawerNavigator } from "./DrawerNavigator";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const { Navigator, Screen } = createStackNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Navigator screenOptions={{ headerShown: false }}>
        <Screen name="Drawer" component={DrawerNavigator} />
      </Navigator>
    </NavigationContainer>
  );
};

export { AppNavigator };
