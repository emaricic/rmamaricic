import {
  Avatar,
  Drawer,
  DrawerItem,
  IndexPath,
  Layout,
  StyleService,
  Text,
  useStyleSheet,
} from "@ui-kitten/components";
import * as SQLite from "expo-sqlite";
import React, { useState, useEffect } from "react";
import { Ionicons } from "@expo/vector-icons";
import { SafeAreaView } from "react-native-safe-area-context";
import { View } from "react-native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { BottomNavigator } from "./BottomNavigator";
import Hosts from "../screens/Hosts";
import { Picker } from "@react-native-picker/picker";

const DrawerController = createDrawerNavigator();
const db = SQLite.openDatabase("database");

const DrawerContent = ({ navigation, state }) => {
  const styles = useStyleSheet(themedStyles);

  const Header = () => (
    <Layout style={styles.header}>
      <View style={styles.profileContainer}>
        <Avatar
          size="giant"
          color="black"
          source={require("../../assets/award-outline.svg")}
        />
        <Text style={styles.profileName} category="h6">
          Turizam
        </Text>
      </View>
    </Layout>
  );

  return (
    <SafeAreaView>
      <Drawer
        header={Header}
        selectedIndex={new IndexPath(state.index)}
        onSelect={(index) => navigation.navigate(state.routeNames[index.row])}
      >
        <DrawerItem title="Iznajmljivači" />
      </Drawer>
    </SafeAreaView>
  );
};

const DrawerNavigator = ({ navigation }) => {
  const [hosts, setHosts] = useState([]);
  const [currentHost, setCurrentHost] = useState("");

  const [tabName, setTabName] = useState("Gosti");

  const fetchData = () => {
    db.transaction((tx) => {
      // sending 4 arguments in executeSql
      tx.executeSql(
        // let innerJoin = await this.ExecuteQuery("SELECT users.id, users.first_name, users.last_name, c.country_name FROM users INNER JOIN country c on c.user_id = users.id
        "SELECT * FROM hosts",
        null, // passing sql query and parameters:null
        // success callback which sends two things Transaction object and ResultSet Object
        (txObj, { rows: { _array } }) => setHosts({ data: _array }),
        // failure callback which sends two things Transaction object and Error
        (txObj, error) => console.log("Error ", error)
      ); // end executeSQL
    }); // end transaction
  };
  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    if (hosts.data) {
      setCurrentHost(`${hosts.data[0].id}`);
    }
  }, [hosts]);

  useEffect(() => {
    console.log(hosts);
  }, [hosts]);

  return (
    <DrawerController.Navigator
      initialRouteName="Home"
      drawerContent={(props) => <DrawerContent {...props} />}
    >
      <DrawerController.Screen
        name="Hosts"
        component={Hosts}
        options={{
          headerTitle: "Iznajmljivači",
          headerStyle: { backgroundColor: "#8CA0CE" },
          headerLeft: () => (
            <Ionicons
              name="arrow-back-outline"
              onPress={() => navigation.navigate("Home")}
              size={28}
              style={{ marginLeft: 10 }}
              color="black"
            />
          ),
        }}
      />
      <DrawerController.Screen
        options={{
          headerTitle: tabName,
          gestureEnabled: false,
          headerStyle: { backgroundColor: "#8CA0CE" },

          headerRight: () => (
            <View
              style={{
                display: "flex",
                width: 200,
                flex: 1,
              }}
            >
              {hosts.data ? (
                <Picker
                  selectedValue={currentHost}
                  onValueChange={(itemValue) => {
                    setCurrentHost(itemValue);
                  }}
                  style={{ flex: 1, marginRight: 5 }}
                >
                  {hosts.data.map((item, index) => {
                    return (
                      <Picker.Item
                        value={item.id}
                        label={item.name}
                        key={index}
                      />
                    );
                  })}
                </Picker>
              ) : null}
            </View>
          ),
        }}
        name="Home"
        children={() => (
          <BottomNavigator currentHost={currentHost} setTabName={setTabName} />
        )}
      />
    </DrawerController.Navigator>
  );
};

const themedStyles = StyleService.create({
  header: {
    height: 128,
    paddingHorizontal: 16,
    justifyContent: "center",
  },
  profileContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  profileName: {
    marginHorizontal: 16,
  },
  icon: {
    width: 22,
    height: 22,
    marginRight: 8,
  },
});

export { DrawerNavigator };
